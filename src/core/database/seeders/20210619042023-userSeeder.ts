'use strict';
const { v4: uuidv4 } = require('uuid')
module.exports = {
    up: async (queryInterface, Sequelize) => {

        await queryInterface.bulkInsert('users', [{
            id: uuidv4(),
            name: 'John Doe',
            email: "eluks@mac.com",
            password: "password",
            createdAt: new Date().toISOString (),
            updatedAt: new Date().toISOString(),
        }], {});
    },

    down: async (queryInterface, Sequelize) => {
        await queryInterface.bulkDelete('users', null, {});
    }
};
