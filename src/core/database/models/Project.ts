'use strict';

import {Column, Model, Table} from "sequelize-typescript";

interface UserAttributes {
    id?: string;
    title: string;
    status: string;
}

@Table
export class Project extends Model<UserAttributes, Project> implements UserAttributes {
    @Column
    title!: string;
    status!: string;

}
