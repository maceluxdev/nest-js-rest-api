'use strict';
import {
    Column, ForeignKey,
    Model, Table
} from 'sequelize-typescript';
import {User} from "./User";
import {Project} from "./Project";

interface ProjectAssignmentsAttributes {
    id?: number;
    userId: string;
    projectId: number;
}

@Table({
   tableName: 'project_assignments'
})
export class ProjectAssignment extends Model<ProjectAssignmentsAttributes, ProjectAssignment> implements ProjectAssignmentsAttributes {

    @ForeignKey(() => User)
    @Column
    userId: string

    @ForeignKey(() => Project)
    @Column
    projectId: number;

}
