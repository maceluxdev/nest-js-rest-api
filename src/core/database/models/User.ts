'use strict';
import {
    Table,
    Model,
    Column,
    Scopes,
    DefaultScope,
    HasMany,
    BelongsToMany,
    HookOptions,
    BeforeCreate
} from "sequelize-typescript";
import {Project} from './Project';
import * as bcrypt from 'bcrypt';
import {ProjectAssignment} from "./ProjectAssignments";

interface UserAttributes {
    id: string;
    name: string;
    email: string;
    password: string;
}

@DefaultScope(() => ({
    attributes: ['id', 'name', 'email']
}))
@Table({
    tableName: 'users'
})
export class User extends Model<User, UserAttributes> {

    @Column
    name: string;

    @Column
    email: string;

    @Column
    password!: string;

    @BeforeCreate
    static async hashPassword(instance: User) {
        instance.password = await bcrypt.hash(instance.password, 10);
    }

    @BelongsToMany(() => Project, () => ProjectAssignment)
    projects: Project[]

}
