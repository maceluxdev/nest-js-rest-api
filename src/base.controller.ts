import {Controller, Get} from '@nestjs/common';
import {AppService} from './app.service';

@Controller()
export class BaseController {
    constructor() {
    }

    public responseJson(message?: string, data?: {}, statusCode: number = 200): {} {
        return {
            message: message,
            data: data,
            statusCode: statusCode
        };
    };
}
