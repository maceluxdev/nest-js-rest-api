export class CreateProductDto{ 
    public id: string;
    public title: string;
    public description: string;
    public price: number
}