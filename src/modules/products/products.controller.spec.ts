import { Test, TestingModule } from '@nestjs/testing';
import {ProductsController} from "./products.controller";
import {ProductsService} from "./products.service";


describe('AppController', () => {
  let productController: ProductsController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [ProductsController],
      providers: [ProductsService],
    }).compile();

     productController = app.get<ProductsController>(ProductsController);
  });

  describe('Module', () => {
    it('should return "Object!"', () => {
      expect(productController.getAllProducts()).toStrictEqual( []);
    });
  });
});
