import { Injectable,NotFoundException } from '@nestjs/common';
import {Product} from "./model/product";

@Injectable()
export class ProductsService {
    products: Product[] = [];

    createProduct(title: string , description: string, price: number) {
        let id = Math.random().toString();
        const newProduct =  new Product(id,title, description, price);
        this.products.push(newProduct);
        return id;
    }

    getProducts(){
        return [...this.products]
    }

    getSingleProduct(productId: string){
        const product = this.findProduct(productId);
        return {...product};
    }

    private findProduct(productId: string) {
        const product = this.products.find(prod => prod.id === productId);
        if (!product) {
            throw new NotFoundException('Could not find product');
        }
        return product;
    }

    updateProduct(productId: string, title: string , description: string, price: number) {
        console.log(productId);
        const product = this.findProduct(productId);
    }
}
