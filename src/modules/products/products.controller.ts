import {Controller, Get, Post, Body, Param, Patch} from '@nestjs/common';
import {ProductsService} from "./products.service";

@Controller('products')
export class ProductsController {
    constructor(private readonly productService: ProductsService) {
    }

    @Get()
    getAllProducts(): {} {
        return this.productService.getProducts();
    }

    @Post()
    async addProduct(@Body('title') prodTitle: string,@Body('description') prodDescription: string, @Body('price') prodPrice: number ) {
        const generatedId =  this.productService.createProduct(prodTitle,prodDescription, prodPrice);
       console.log(generatedId);
        return {id: generatedId};
    }

    @Get(':id')
    getSingleProduct(@Param('id') prodId: string): {} {
        return this.productService.getSingleProduct(prodId);
    }
}
