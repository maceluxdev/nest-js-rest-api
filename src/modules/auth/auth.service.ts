
import { Injectable } from '@nestjs/common';
import { UserService } from '../user/user.service';
import {JwtService} from "@nestjs/jwt";

@Injectable()
export class AuthService {
    constructor(private usersService: UserService, private jwtService: JwtService) {}

    async validateUser(username: string, pass: string): Promise<any> {
        const user = await this.usersService.findOne(username);
        if (user && user.password === pass) {
            const { password, ...result } = user;
            return result;
        }
        return null;
    }

    // async login(user: any) {
    //     const payload = { username: user.username, sub: user.userId };
    //     return {
    //         access_token: this.jwtService.sign(payload),
    //     };
    // }

    // public async login(user) {
    //     const token = await this.generateToken(user);
    //     return { user, token };
    // }

    async login(user: any) {
        const payload = { username: user.username, sub: user.userId };
        return {
            access_token: this.jwtService.sign(payload),
        };
    }

    public async create(user) {
        // hash the password
        const pass = await this.hashPassword(user.password);

        // create the user
        // const newUser = await this.userService.create({ ...user, password: pass });
        //
        // // tslint:disable-next-line: no-string-literal
        // const { password, ...result } = newUser['dataValues'];
        //
        // // generate token
        // const token = await this.generateToken(result);
        //
        // // return the user and the token
        // return { user: result, token };
    }

    private async generateToken(user) {
        const token = await this.jwtService.signAsync(user);
        return token;
    }

    private async hashPassword(password) {
        // const hash = await bcrypt.hash(password, 10);
        // return hash;
    }

    // private async comparePassword(enteredPassword, dbPassword) {
    //     const match = await bcrypt.compare(enteredPassword, dbPassword);
    //     return match;
    // }
}
