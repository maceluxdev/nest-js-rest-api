import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthService } from './auth.service';
import {User} from "../../core/database/models/User";

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
    constructor(private authService: AuthService) {
        super({ usernameField: 'username'});
    }

    async validate(username: string, password: string): Promise<User> {
        console.log(username);
        const user = await this.authService.validateUser(username, password);
        if (!user) {
            throw new UnauthorizedException('invalid user credentials');
        }
        return user;
    }
}
