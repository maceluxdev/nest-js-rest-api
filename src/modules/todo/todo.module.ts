import { Module } from '@nestjs/common';
import { TodoController } from './todo.controller';
import { TodoService } from './todo.service';
import {ProductsService} from "../products/products.service";

@Module({
  controllers: [TodoController],
  providers: [ProductsService]
})
export class TodoModule {}
