import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
// import { todos } from 'src/mock/todos.mock';
import { v4 as uuid } from 'uuid';
import {Todo} from "./entity/TodoEntity";
import {TodoDto} from "./dto/TodoDto";
import {toPromise} from "../../shared/utils";
// import {toTodoDto} from "../../shared/mapper";
import {TodoCreateDto} from "./dto/TodoCreateDto";
import {TodoListDto} from "./dto/TodoListDto";

// noinspection TypeScriptValidateTypes
@Injectable()
export class TodoService {
    // assign imported data to local variable
    // todos: Todo[] = todos;

    // async getOneTodo(id: string): Promise<TodoDto> {
    //     const todo = this.todos.find(todo => todo.id === id);
    //
    //     if (!todo) {
    //         throw new HttpException(`Todo item doesn't exist`, HttpStatus.BAD_REQUEST);
    //     }
    //
    //     return toPromise(toTodoDto(todo));
    // }

    // async createTodo(todoDto: TodoCreateDto):Promise<TodoDto>{
    //     const { name, description } = todoDto;
    //
    //     // const todo: Todo = {
    //     //     id: uuid(),
    //     //     name,
    //     //     description,
    //     // };
    //
    //     // this.todos.push(todo);
    //     // return toPromise(toTodoDto(todo));
    // }
    //
    // // async getAllTodo(): Promise<TodoListDto>{
    // //     let todo: TodoListDto[] = [...this.todos];
    // //     return toPromise(todo);
    // // }

}
