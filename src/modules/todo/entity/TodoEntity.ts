import { Task } from "./TaskEntity";
import {Column, DataType, HasMany, Model, Table} from "sequelize-typescript";

@Table
export class Todo extends Model<Todo> {
    @Column({
        type: DataType.UUID,
        allowNull: false,
        primaryKey: true,
    })
    id: bigint;

    @Column({
        type: DataType.STRING,
        allowNull: false,
    })
    name: string;

    @Column({
        type: DataType.TEXT,
        allowNull: false,
    })
    description: string;

    @Column({
        type: DataType.DATE,
        field: "created_at"
    })
    createdAt: true;

    @Column({
        type: DataType.DATE,
        field: "updated_at"
    })
    updatedAt: true;

    @HasMany(() => Task)
    user: Task;
}