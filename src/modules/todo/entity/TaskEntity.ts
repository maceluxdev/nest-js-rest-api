import {Todo } from "./TodoEntity";
import {BelongsTo, Column, DataType, ForeignKey, Model, Table} from "sequelize-typescript";

@Table
export class Task extends Model<Task> {
    @Column({
        type: DataType.UUID,
        allowNull: false,
        primaryKey: true,
    })
    id: bigint;

    @Column({
        type: DataType.STRING,
        allowNull: false,
    })
    name: string;

    @Column({
        type: DataType.TEXT,
        allowNull: false,
    })
    description: string;

    @Column({
        type: DataType.DATE,
        field: "created_at"
    })
    createdAt: true;

    @Column({
        type: DataType.DATE,
        field: "updated_at"
    })
    updatedAt: true;

    @ForeignKey(() => Todo)
    @Column({
        type: DataType.BIGINT,
        allowNull: false,
        field: "todo_id"
    })
    todoID: number;

    @BelongsTo(() => Todo)
    todo: Todo;
}