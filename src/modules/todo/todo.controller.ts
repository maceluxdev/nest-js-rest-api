import {Body, Controller, Get, Param, Post, Put, Delete} from '@nestjs/common';
import {TodoCreateDto} from "./dto/TodoCreateDto";
import {TodoDto} from "./dto/TodoDto";
import {TodoService} from "./todo.service";
import {ProductsService} from "../products/products.service";

@Controller('todo')
export class TodoController {
    // inject TodoService
    // constructor(private readonly todoService: TodoService) {}

    // @Get()
    // async findAll(): Promise<TodoListDto> {
    //     const todos = await this.todoService.getAllTodo();
    //
    //     return toPromise({ todos });
    // }

    constructor(private readonly productService: ProductsService) {
    }

    @Get()
    getAllProducts(): {} {
        return this.productService.getProducts();
    }

    // @Get(":id")
    // async findOne(@Param("id") id: string): Promise<TodoDto> {
    //     return await this.todoService.getOneTodo(id);
    // }
    //
    // @Post()
    // async create(@Body() todoCreateDto: TodoCreateDto): Promise<TodoDto> {
    //     return await this.todoService.createTodo(todoCreateDto);
    // }

    // @Put(":id")
    // async update(
    //     @Param("id") id: string,
    //     @Body() todoDto: TodoDto
    // ): Promise<TodoDto> {
    //     return await this.todoService.updateTodo(todoDto);
    // }
    //
    // @Delete(":id")
    // async destroy(@Param("id") id: string): Promise<TodoDto> {
    //     return await this.todoService.destoryTodo(id);
    // }
}
