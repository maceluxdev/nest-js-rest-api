import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import {BaseController} from "../../base.controller";

@Module({
  controllers: [UserController],
  providers: [UserService, BaseController],
  exports: [UserService]
})
export class UserModule {}
