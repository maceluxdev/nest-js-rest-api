import {Body, Controller, Get, Post, HttpStatus} from '@nestjs/common';
import {UserService} from "./user.service";
import {BaseController} from "../../base.controller";

@Controller('users')
export class UserController{

    constructor(protected readonly userService : UserService,   private baseController: BaseController) {
        this.baseController = new BaseController();
    }

    @Get()
    async findOne( id: string) {
        return await this.userService.findAllUsers();
    }

    @Post()
    async postUser(@Body() request) {
        const data = await this.userService.saveUser();
        return this.baseController.responseJson('resource created', data, 200 );
    }
}
