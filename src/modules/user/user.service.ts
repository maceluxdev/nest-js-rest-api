import {Injectable, ExecutionContext, HttpException, HttpStatus} from '@nestjs/common';
import {User} from "../../core/database/models/User";
import {v4 as uuid} from 'uuid';

export type Users = any;

@Injectable()
export class UserService {
    private readonly users = [
        {
            userId: 1,
            username: 'john',
            password: 'changeme',
        },
        {
            userId: 2,
            username: 'maria',
            password: 'guess',
        },
    ];

    async findOneById(id: number){
       return this.users.find(user => user.userId === id);
       // return await User.findOne({ where : { name: username}, raw: true});
    }

    async findOne(username: string): Promise<Users | undefined> {
       return this.users.find(user => user.username === username);
       // return await User.findOne({ where : { name: username}, raw: true});
    }

    async findAllUsers() {
        // Find all users
        const users = await User.findAll({raw: true});
        console.log(users.every(user => user instanceof User)); // true
        console.log("All users:", JSON.stringify(users, null, 2));
    }

    async saveUser(): Promise<User> {
        try {
            const user = new User({id: uuid(), name: "jude", email:"jude@gmail.com", password: "password"});
            return await user.save();
        } catch (ex) {
            console.log(ex.errors);
            throw new HttpException({
                status: HttpStatus.INTERNAL_SERVER_ERROR,
                error: 'There was a Problem saving this data',
            }, HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }
}
