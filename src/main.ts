import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import 'dotenv/config'

// set .env
const port = process.env.APP_PORT

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    // logger: ['error', 'warn', "debug"],
  });
  // set global prefix
  app.setGlobalPrefix('api/v1');
  await app.listen(port);
}
bootstrap();
